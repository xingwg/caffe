#ifndef CAFFE_SHAPE_INDEX_PATCH_LAYER_HPP_
#define CAFFE_SHAPE_INDEX_PATCH_LAYER_HPP_

#include <vector>

#include "caffe/blob.hpp"
#include "caffe/layer.hpp"
#include "caffe/proto/caffe.pb.h"

namespace caffe {

/**
 * @brief .
 *
 * TODO(dox): .
 */
template <typename Dtype>
class ShapeIndexPatchLayer : public Layer<Dtype> {
 public:
  explicit ShapeIndexPatchLayer(const LayerParameter& param)
      : Layer<Dtype>(param) {}
  virtual void LayerSetUp(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);
  virtual void Reshape(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);

  virtual inline const char* type() const { return "ShapeIndexPatch"; }
  virtual inline int ExactNumBottomBlobs() const { return 2; }

 protected:
  virtual void Forward_cpu(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top);
  //virtual void Forward_gpu(const vector<Blob<Dtype>*>& bottom,
  //    const vector<Blob<Dtype>*>& top);
  virtual void Backward_cpu(const vector<Blob<Dtype>*>& top,
      const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom);
  //virtual void Backward_gpu(const vector<Blob<Dtype>*>& top,
  //    const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom);

  int origin_patch_h_;
  int origin_patch_w_;
  int origin_h_;
  int origin_w_;
  int feat_h_;
  int feat_w_;
  int feat_patch_h_;
  int feat_patch_w_;  
};

}  // namespace caffe

#endif  // CAFFE_SHAPE_INDEX_PATCH_LAYER_HPP_
