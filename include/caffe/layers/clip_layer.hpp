//
// Created by sunc on 20-10-16.
//

#ifndef CAFFE_CLIP_LAYER_HPP
#define CAFFE_CLIP_LAYER_HPP

#include "caffe/blob.hpp"
#include "caffe/layer.hpp"
#include "caffe/proto/caffe.pb.h"
#include <limits>

namespace caffe {
    template<typename Dtype>
    class ClipLayer : public Layer<Dtype> {
    public:
        explicit ClipLayer(const LayerParameter &param)
                : Layer<Dtype>(param) {}

        virtual void LayerSetUp(const vector<Blob<Dtype>*> &bottom, const vector<Blob<Dtype>*> &top);
        virtual void Reshape(const vector<Blob<Dtype>*> &bottom, const vector<Blob<Dtype>*> &top);

        virtual inline const char *type() const { return "Clip"; }
        virtual inline int ExactBottomBlobs() const { return 1; }
        virtual inline int ExactNumTopBlobs() const { return 1; }

    protected:
        virtual void Forward_cpu(const vector<Blob<Dtype>*> &bottom, const vector<Blob<Dtype>*> &top);
        virtual void Forward_gpu(const vector<Blob<Dtype>*> &bottom, const vector<Blob<Dtype>*> &top) {NOT_IMPLEMENTED;}

        virtual void Backward_cpu(const vector<Blob<Dtype>*> &top, const vector<bool> &propagate_down,
                                  const vector<Blob<Dtype>*> &bottom) { NOT_IMPLEMENTED; }
        virtual void Backward_gpu(const vector<Blob<Dtype>*> &top, const vector<bool> &propagate_down,
                                  const vector<Blob<Dtype>*> &bottom) { NOT_IMPLEMENTED; }

        float min_{std::numeric_limits<float>::min()};
        float max_{std::numeric_limits<float>::max()};
    };
}  // namespace caffe
#endif //CAFFE_CLIP_LAYER_HPP
