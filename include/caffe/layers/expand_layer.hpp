//
// Created by Xing on 2022/3/26.
//

#ifndef CAFFE_EXPAND_LAYER_HPP
#define CAFFE_EXPAND_LAYER_HPP

#include "caffe/blob.hpp"
#include "caffe/layer.hpp"
#include "caffe/proto/caffe.pb.h"
namespace caffe {
    template<typename Dtype>
    class ExpandLayer : public Layer<Dtype> {
    public:
        explicit ExpandLayer(const LayerParameter &param)
                : Layer<Dtype>(param) {}

        virtual void LayerSetUp(const vector<Blob<Dtype>*> &bottom, const vector<Blob<Dtype>*> &top);
        virtual void Reshape(const vector<Blob<Dtype>*> &bottom, const vector<Blob<Dtype>*> &top);

        virtual inline const char *type() const { return "Expand"; }
        virtual inline int ExactBottomBlobs() const { return 1; }
        virtual inline int ExactNumTopBlobs() const { return 1; }

    protected:
        virtual void Forward_cpu(const vector<Blob<Dtype>*> &bottom, const vector<Blob<Dtype>*> &top);
        virtual void Forward_gpu(const vector<Blob<Dtype>*> &bottom, const vector<Blob<Dtype>*> &top) {NOT_IMPLEMENTED;}

        virtual void Backward_cpu(const vector<Blob<Dtype>*> &top, const vector<bool> &propagate_down,
                                  const vector<Blob<Dtype>*> &bottom) { NOT_IMPLEMENTED; }
        virtual void Backward_gpu(const vector<Blob<Dtype>*> &top, const vector<bool> &propagate_down,
                                  const vector<Blob<Dtype>*> &bottom) { NOT_IMPLEMENTED; }

    private:
        vector<int> shape_;
    };
}  // namespace caffe

#endif //CAFFE_EXPAND_LAYER_HPP
